<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Models\Persona;
use App\Models\Telefono;
use Response;
use View;
use DB;

class IndexController extends Controller{
    public function enviarFormulario(Request $request){
        //try{
			DB::beginTransaction();
			$input = request()->validate([
                'nombres' => 'required',
                'run' => 'required|min:5',
                'telefono' => 'required'
            ], [
                'run.required' => 'El run es obligatorio',
                'name.required' => 'El nombre es obligatorio',
                'telefono.required' => 'El telefono es obligatorio'
            ]);
    
            $input = request()->all();
    
            $persona = Persona::agregarPersona($input);
            $persona->save();
            $telefono = Telefono::agregarTelefono($persona->run_persona, $input['telefono']);
            $telefono->save();

            DB::commit();
            return View::make("resultado", ["persona"=>$persona, "telefono"=>$telefono]);
		/* }catch(\Exception $ex){
			DB::rollback();
			return back()->with('error', 'Ha ocurrido un error');
		} */

    }
}