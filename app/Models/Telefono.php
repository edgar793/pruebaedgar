<?php

namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Funciones;
use Carbon\Carbon;

class Telefono extends Model{

	protected $table = 'telefono';
	public $timestamps = false;
    protected $primaryKey = 'id_telefono';
    
    public static function agregarTelefono($id, $numero){
        $telefono = new Telefono();
        $telefono->run_persona = $id;
        $telefono->telefono = $numero;

        return $telefono;
    }



}