<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Funciones;
use Carbon\Carbon;

class Persona extends Model{

	protected $table = 'persona';
	public $timestamps = false;
	protected $primaryKey = 'run_persona';

    public static function agregarPersona($data){
        $persona = new Persona();
        $persona->run_persona = $data['run'];
        $persona->nombres = $data['nombres'];

        return $persona;
    }

}