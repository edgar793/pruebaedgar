<!DOCTYPE html> 
<html> 
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Prueba</title>
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
        <script src="{{asset('js/index.js')}}"></script>
    </head> 
    <body> 
        <div class="container" style="margin-top:80px;">
            <legend class="col-sm-4 offset-sm-4" style="text-align:center;">Formulario</legend>
            {{ Form::open(array('url' => 'enviarFormulario', 'method' => 'post', 'role' => 'form', 'id' => 'formPrueba')) }}
            <div class="row">
                <div class="col-sm-4 offset-sm-4">
                    <div class="form-group"> 
                        <label for="nombres">Nombres</label>
                        {{Form::text('nombres', null,array('id' => 'nombres', 'class' => 'form-control'))}}
                    </div>
                </div> 
            </div>

            <div class="row">
                <div class="col-sm-4 offset-sm-4">
                    <div class="form-group"> 
                        <label for="run">Run</label>
                        {{Form::text('run', null,array('id' => 'run', 'class' => 'form-control'))}}
                        {{-- @if ($errors->has('run'))
                            <span class="text-danger">{{ $errors->first('run') }}</span>
                        @endif --}}
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-4 offset-sm-4">
                    <div class="form-group"> 
                        <label for="telefono">Teléfono</label>
                        {{Form::text('telefono', null,array('id' => 'telefono', 'class' => 'form-control'))}}
                    </div>
                </div>
            </div>

            @if(Session::has('errors'))
                <div style="border: 1px solid; margin-bottom:10px;" class="col-sm-4 offset-sm-4">
                    <ul>
                        @foreach($errors->all() as $errores)
                        <li>{{$errores}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(Session::has('error'))

                <div class="alert alert-danger col-sm-4 offset-sm-4">

                {{ Session::get('error') }}

                @php
                    Session::forget('error');
                @endphp

                </div>

	        @endif
            <div class="col-sm-4 offset-sm-4">
                <button type="submit" class="btn btn-primary" style="width:100%;">Enviar Datos</button> 
            </div>
            {{ Form::close() }}
        </div>
    </body>
</html> 
