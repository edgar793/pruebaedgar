<!DOCTYPE html> 
<html> 
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Prueba</title>
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/style.css')}}" rel="stylesheet">
        <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
        <script src="{{asset('js/index.js')}}"></script>
    </head> 
    <body> 
        <div class="container" style="margin-top:80px;">
            <div class="row">
                <div class="col-sm-4 offset-sm-4">
                    <label style="color:#009E0F; text-align:center;">SE REGISTRÓ CORRECTAMENTE:</label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 offset-sm-4" style="border:1px solid;">
                    <ul>
                        <li>Nombres: {{$persona->nombres}}</li>
                        <li>Run: {{$persona->run_persona}}</li>
                        <li>Teléfono: {{$telefono->telefono}}</li>
                        <li>Fecha: {{date('d-m-Y')}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html> 
