<!DOCTYPE html>
<html lang="es">
<head>

    
    @yield("css")
    @yield("script")
</body>
</html>

<!DOCTYPE html> 
<html> 
    <head>
        <title>@yield("titulo")</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
        
        <script src="{{asset('js/index.js')}}"></script>
    </head> 
    <body> 
        <div class="container">
            {{ Form::open(array('url' => 'enviarFormulario', 'method' => 'post', 'role' => 'form', 'id' => 'formPrueba', 'style' => 'padding-left: 15px;')) }}
            <div class="form-group"> 
                <label for="nombres">Nombres</label>
                {{Form::text('nombres', null,array('id' => 'nombres', 'class' => 'form-control'))}}
            </div> 

            <div class="form-group"> 
                <label for="run">Run</label>
                {{Form::text('run', null,array('id' => 'run', 'class' => 'form-control'))}}
            </div> 

            <div class="form-group"> 
                <label for="telefono">Teléfono</label>
                {{Form::text('telefono', null,array('id' => 'telefono', 'class' => 'form-control'))}}
            </div> 

            <button type="submit" class="btn btn-primary">Enviar Datos</button> 
            {{ Form::close() }}
        </div>
    </body>
</html> 


@extends("template")

@section("titulo")
Formulario
@stop

@section("script")
@stop


@section("section")
@stop

