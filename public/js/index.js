var esRutValido = function(ruti, dvi) {
    var rut = ruti + "-" + dvi;
    if (rut.length < 7)
        return(false)
    i1 = rut.indexOf("-");
    dv = rut.substr(i1 + 1);
    dv = dv.toUpperCase();
    nu = rut.substr(0, i1);

    cnt = 0;
    suma = 0;
    for (i = nu.length - 1; i >= 0; i--) {
        dig = nu.substr(i, 1);
        fc = cnt + 2;
        suma += parseInt(dig) * fc;
        cnt = (cnt + 1) % 6;
    }
    dvok = 11 - (suma % 11);
    if (dvok == 11)
        dvokstr = "0";
    if (dvok == 10)
        dvokstr = "K";
    if ((dvok != 11) && (dvok != 10))
        dvokstr = "" + dvok;
    if (dvokstr == dv)
        return(true);
    else
        return(false);
}

$(function(){
    //alert();
    $("#formPrueba").bootstrapValidator({
		excluded: ':disabled',
		framework: 'bootstrap',
		verbose: false,
		fields: {
			nombres: {
				validators: {
					notEmpty: {
						message: 'Debe ingresar el nombre'
					}
				}
            },
            run: {
				validators: {
					notEmpty: {
						message: 'debe ingresar el rut'
                    },
                    callback: {
                        callback: function(value, validator, $field){
                            if (value === '') {
                                return true;
                            }
                            var rut = value.substring(0, value.length - 1);
                            rut=rut.replace(/\./g, "").replace("-", "");
                            var dv = value[value.length - 1];
                            var valido = esRutValido(rut, dv);
                            if (!valido) {
                                return {
                                    valid: false,
                                    message: 'Rut no coincide con dígito verificador'
                                }
                            }
                            return true;
                        }
                    }
                }
            },
            telefono: {
				validators: {
					notEmpty: {
						message: 'Debe ingresar el teléfono'
                    },
                    integer: {
						message: 'Debe ingresar números'
					}
				}
			}
			
		},
	}).on('err.field.bv', function (e, data) {
		if (data.bv.getSubmitButton()) data.bv.disableSubmitButtons(false);
	}).on('success.field.bv', function (e, data) {
        if (data.bv.getSubmitButton()) data.bv.disableSubmitButtons(false);
	}).on("success.form.bv", function (evt) {
		//evt.preventDefault(evt);
		//var $form = $(evt.target);
        //alert();
        /* $.ajax({
            url: $form.prop("action"),
            data: $form.serialize(),
            type: $form.prop("method"),
            dataType: "json",
            success: function(data){
                console.log("buena: ", data);
                $("#cargandiwi").hide();
                if(data.error) bootbox.alert("<h4>"+data.error+"</h4>");
                if(data.exito) bootbox.alert("<h4>"+data.exito+"</h4>", function(){
                    obtenerContactoPaciente();
                    ///////////////////////////////////////////////////////////////
                    //Solucion temporal al no poder hacer refresh en el typeahead//
                    ///////////////////////////////////////////////////////////////
                    location.reload();
                });
            }, 
            error: function(error){
                console.log("error", error);
            }
        }); */
	});
});